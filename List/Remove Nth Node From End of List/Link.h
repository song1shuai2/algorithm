//
//  Link.h
//  Remove Nth Node From End of List
//
//  Created by shuai song on 10/24/14.
//  Copyright (c) 2014 shuai song. All rights reserved.
//

#ifndef __Remove_Nth_Node_From_End_of_List__Link__
#define __Remove_Nth_Node_From_End_of_List__Link__

#include <stdio.h>
#include <iostream>
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL)
    {
        
    }
};
class Link
{

   
public:
    ListNode * head;
    Link()
    {
      
    }
    void Create(int number);
    void ShowList();
    void removeNthFromEnd(int n);
    void Insert(int index,int value);
    void DeleteDuplicates();
    void RotateList(int k);
    static void AddTwoList(Link & list1,Link &list2)
    {
        
    }
    
    
    
};

#endif /* defined(__Remove_Nth_Node_From_End_of_List__Link__) */
