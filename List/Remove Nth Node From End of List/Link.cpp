//
//  Link.cpp
//  Remove Nth Node From End of List
//
//  Created by shuai song on 10/24/14.
//  Copyright (c) 2014 shuai song. All rights reserved.
//

#include "Link.h"
void Link::Create(int number)
{
    ListNode *ptr=NULL;
    
    for (int n=0; n<number; n++)
    {
        ListNode  *node = new ListNode(n);
        if (head==NULL)
        {
            head =node;
            ptr = node;
            continue;
        }
     
        ptr->next=node;
        ptr = ptr->next;
    }
    
}
void Link::ShowList()
{
     ListNode *ptr=head;
    
    while (ptr!=NULL)
    {
        std::cout<<ptr->val<<"->";
        ptr=ptr->next;
    }
    std::cout<<"NULL"<<std::endl;
}

void Link::removeNthFromEnd(int n)
{
    ListNode * ptr = head;
    ListNode * end = head;
    ListNode * pre = head;
    for (int i= 0; i<n; i++)
    {
        end = end->next;
    }
    while (end!=NULL)
    {
        end=end->next;
        pre = ptr;
        ptr= ptr->next;
    }
    if (ptr == head)
    {
        head = head->next;
    }
    else
    {
        pre->next = ptr->next;
    }
    
    pre = NULL;
    end = NULL;
    pre = NULL;
    
    delete ptr;
}

void Link::Insert(int index,int value)
{
    ListNode * newNode = new ListNode(value);
    ListNode * post =NULL,*cur = head;
    for (int i = 0; i < index; i++)
    {
        cur= cur->next;
    }
    post = cur->next;
    cur->next = newNode;
    newNode->next = post;
    
}
void Link::DeleteDuplicates()
{
    
    if (head == NULL||head->next==NULL)
    {
        //return head;
        return;
    }
    ListNode * cur = head;
    ListNode * post;
    while (cur!=NULL)
    {
        
        while (cur->next!=NULL&&cur->val==cur->next->val)
        {
            
            ListNode * tmp = cur->next;
            cur->next=cur->next->next;
            delete tmp;
            
        }
        cur=cur->next;
        
    }
    

}
void Link::RotateList(int k)
{
    if (head == NULL||k==0)
    {
        return ;
    }
    
    ListNode * end=head;
    ListNode * cur=head;
    //ListNode * pre =head;
    
    for (int i=0; i<k; i++)
    {
        if (end->next!=NULL)
        {
            end=end->next;
        }
        else
        {
            end=head;
        }
    }

    while (end->next!=NULL)
    {
        //pre=cur;
        cur=cur->next;
        end=end->next;
    }
    
    end->next = head;
    head=cur->next;
    cur->next = NULL;
}