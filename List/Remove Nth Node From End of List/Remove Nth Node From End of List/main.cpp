//
//  main.cpp
//  Remove Nth Node From End of List
//
//  Created by shuai song on 10/24/14.
//  Copyright (c) 2014 shuai song. All rights reserved.
//

#include <iostream>
/**
 * Definition for singly-linked list.
 */

#include "Link.h"

int main(int argc, const char * argv[])
{
    
    Link testLink;
    testLink.Create(5);
   // testLink.head->val =1;
//    testLink.ShowList();
//   // testLink.removeNthFromEnd(1);
//   // testLink.ShowList();
//    testLink.Insert(2, 2);
   // testLink.Insert(0, 1);
//    testLink.ShowList();
    //testLink.DeleteDuplicates();
    testLink.ShowList();
    testLink.RotateList(1);
    testLink.ShowList();
    return 0;
}

